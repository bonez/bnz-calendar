(function() {
  'use strict';
  var Calendar, calendarBackdropClassName, calendarBackdropEl, calendarClassName, calendarEl, ce, dateDiff, delegateEvents, disabledFrom, disabledTill, event, focusedEl, getDateAfterSomeDays, getTomorrowDate, getYesterdayDate, hideCalendar, makeCalendar, makeCalendarFromData, makeCalendarFromSelectDataByMonth, makeCalendarFromSelectDataByYear, month_days, month_names, render, selectDate, selectDateFromData, showCalendar, today, undelegateEvents, weekdays;
  month_names = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  weekdays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
  month_days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  calendarClassName = 'bnz-calendar';
  calendarBackdropClassName = "" + calendarClassName + "-backdrop";
  today = new Date();
  ce = function(tagName, className, html) {
    var el;
    el = document.createElement(tagName);
    if (className) {
      el.className = className;
    }
    if (html) {
      el.innerHTML = html;
    }
    return el;
  };
  focusedEl = null;
  calendarEl = null;
  calendarBackdropEl = null;
  disabledFrom = null;
  disabledTill = null;
  event = null;
  dateDiff = function(date1, date2) {
    return Math.ceil(((new Date(date2)).getTime() - (new Date(date1)).getTime()) / (1000 * 3600 * 24));
  };
  getDateAfterSomeDays = function(date, some) {
    var myDate;
    if (typeof date === 'number') {
      myDate = new Date(this.getDateFormatted());
      some = date;
    } else {
      myDate = new Date(date);
    }
    myDate.setDate(myDate.getDate() + some);
    return this.getDateFormatted(myDate);
  };
  getTomorrowDate = function(date) {
    if (date == null) {
      date = this.getDateFormatted();
    }
    return getDateAfterSomeDays.call(this, date, 1);
  };
  getYesterdayDate = function(date) {
    if (date == null) {
      date = this.getDateFormatted();
    }
    return getDateAfterSomeDays.call(this, date, -1);
  };
  Calendar = (function() {
    function Calendar(options) {
      var input, inputs, timer, _i, _len;
      if (options == null) {
        options = {
          selector: '.datepicker',
          disabledFrom: null,
          disabledTill: null
        };
      }
      event = new CustomEvent('bnzCalendarDateChange', {
        bubbles: true,
        cancelable: true
      });
      disabledFrom = (function() {
        switch (options.disabledFrom) {
          case 'today':
            return this.getDateFormatted();
          case 'tomorrow':
            return getTomorrowDate.call(this);
          case 'yesterday':
            return getYesterdayDate.call(this);
          default:
            return options.disabledFrom;
        }
      }).call(this);
      disabledTill = typeof options.disabledTill === 'number' ? getDateAfterSomeDays.call(this, disabledFrom, options.disabledTill) : options.disabledTill;
      inputs = document.querySelectorAll(options.selector);
      if (inputs.length <= 0) {
        return;
      }
      timer = null;
      for (_i = 0, _len = inputs.length; _i < _len; _i++) {
        input = inputs[_i];
        input.addEventListener('focus', (function(_this) {
          return function(e) {
            window.clearTimeout(timer);
            return showCalendar.call(_this, e);
          };
        })(this), false);
        input.addEventListener('keydown', (function(_this) {
          return function(e) {
            if (e.keyCode !== 9) {
              e.preventDefault();
            }
            if (e.keyCode === 27) {
              hideCalendar();
              return e.target.blur();
            }
          };
        })(this), false);
      }
      return;
    }

    Calendar.prototype.getDateFormatted = function(d) {
      var dd, mm, year;
      if (d == null) {
        d = new Date();
      }
      year = d.getFullYear().toString();
      mm = (d.getMonth() + 1).toString();
      mm = mm[1] ? mm : "0" + mm[0];
      dd = d.getDate().toString();
      dd = dd[1] ? dd : "0" + dd[0];
      return "" + year + "-" + mm + "-" + dd;
    };

    Calendar.prototype.dateDiff = dateDiff;

    Calendar.prototype.getDateAfterSomeDays = getDateAfterSomeDays;

    Calendar.prototype.getTomorrowDate = getTomorrowDate;

    Calendar.prototype.getYesterdayDate = getYesterdayDate;

    return Calendar;

  })();
  makeCalendar = function(year, month, day) {
    var class_name, currDate, current_year, d, dayEl, days_in_this_month, emptyDiv, first_day, flag, header, i, index, j, mon, naviDiv, naviNext, naviPrev, next_month, next_month_year, option, previous_month, previous_month_year, selectMonth, selectYear, selectsDiv, start_day, value, w, w0, week, weekEl, weekday, yea, _i, _len;
    year = parseInt(year, 10);
    month = parseInt(month, 10);
    day = parseInt(day, 10);
    next_month = month + 1;
    next_month_year = year;
    if (next_month >= 12) {
      next_month = 0;
      next_month_year++;
    }
    previous_month = month - 1;
    previous_month_year = year;
    if (previous_month < 0) {
      previous_month = 11;
      previous_month_year--;
    }
    calendarEl.innerHTML = '';
    naviDiv = ce('div', 'navi');
    calendarEl.appendChild(naviDiv);
    naviPrev = ce('div', null, '&lt;');
    naviPrev.setAttribute('data-navi', "" + previous_month_year + "-" + previous_month);
    naviPrev.setAttribute('title', "" + month_names[previous_month] + " " + previous_month_year);
    naviDiv.appendChild(naviPrev);
    selectsDiv = ce('div', 'calendar-title');
    naviDiv.appendChild(selectsDiv);
    naviNext = ce('div', null, '&gt;');
    naviNext.setAttribute('data-navi', "" + next_month_year + "-" + next_month);
    naviNext.setAttribute('title', "" + month_names[next_month] + " " + next_month_year);
    naviDiv.appendChild(naviNext);
    selectMonth = ce('select', 'calendar-month');
    selectMonth.setAttribute('data-year', year);
    selectsDiv.appendChild(selectMonth);
    for (index = _i = 0, _len = month_names.length; _i < _len; index = ++_i) {
      value = month_names[index];
      option = ce('option', null, value);
      option.value = index;
      if (index === month) {
        option.setAttribute('selected', 'selected');
      }
      selectMonth.appendChild(option);
    }
    selectYear = ce('select', 'calendar-year');
    selectYear.setAttribute('data-month', month);
    selectsDiv.appendChild(selectYear);
    current_year = today.getYear();
    if (current_year < 1900) {
      current_year += 1900;
    }
    i = current_year - 70;
    while (i < current_year + 15) {
      option = ce('option', null, i);
      option.value = i;
      if (i === year) {
        option.setAttribute('selected', 'selected');
      }
      selectYear.appendChild(option);
      i++;
    }
    header = ce('div', 'header');
    calendarEl.appendChild(header);
    weekday = 0;
    while (weekday < 7) {
      week = ce('div', null, weekdays[weekday]);
      header.appendChild(week);
      weekday++;
    }
    first_day = new Date(year, month, 0);
    start_day = first_day.getDay();
    d = 1;
    flag = 0;
    month_days[1] = year % 4 === 0 ? 29 : 28;
    days_in_this_month = month_days[month];
    i = 0;
    while (i <= 5) {
      if (w >= days_in_this_month) {
        break;
      }
      weekEl = ce('div', 'week');
      calendarEl.appendChild(weekEl);
      j = 0;
      while (j < 7) {
        if (d > days_in_this_month) {
          flag = 0;
        } else {
          if (j >= start_day && !flag) {
            flag = 1;
          }
        }
        if (flag) {
          w = d;
          w0 = w;
          mon = month + 1;
          if (w0 < 10) {
            w0 = "0" + w;
          }
          if (mon < 10) {
            mon = "0" + mon;
          }
          class_name = weekdays[j].toLowerCase();
          yea = today.getYear();
          if (yea < 1900) {
            yea += 1900;
          }
          if (yea === year && today.getMonth() === month && today.getDate() === d) {
            class_name = ' today';
          }
          if (day === d) {
            class_name += ' selected';
          }
          currDate = "" + yea + "-" + mon + "-" + (("" + d)[1] ? d : "0" + ("" + d)[0]);
          if (0 <= dateDiff(currDate, disabledFrom)) {
            class_name += ' disabled';
          }
          if (0 > dateDiff(currDate, disabledTill)) {
            class_name += ' disabled';
          }
          dayEl = ce('div', class_name, w);
          dayEl.setAttribute('data-date', "" + year + "-" + mon + "-" + w0);
          weekEl.appendChild(dayEl);
          d++;
        } else {
          emptyDiv = ce('div', null, '&nbsp;');
          weekEl.appendChild(emptyDiv);
        }
        j++;
      }
      i++;
    }
    delegateEvents.call(this);
  };
  delegateEvents = function() {
    var el, _i, _j, _len, _len1, _ref, _ref1;
    _ref = calendarEl.querySelectorAll('[data-navi]');
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      el = _ref[_i];
      el.addEventListener('click', makeCalendarFromData, false);
    }
    calendarEl.querySelector('[data-year]').addEventListener('change', makeCalendarFromSelectDataByYear, false);
    calendarEl.querySelector('[data-month]').addEventListener('change', makeCalendarFromSelectDataByMonth, false);
    _ref1 = calendarEl.querySelectorAll('[data-date]:not(.disabled)');
    for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
      el = _ref1[_j];
      el.addEventListener('mousedown', selectDateFromData, false);
    }
    return calendarBackdropEl.addEventListener('click', hideCalendar);
  };
  undelegateEvents = function() {
    var el, _i, _j, _len, _len1, _ref, _ref1;
    _ref = calendarEl.querySelectorAll('[data-navi]');
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      el = _ref[_i];
      el.removeEventListener('click', makeCalendarFromData);
    }
    calendarEl.querySelector('[data-year]').removeEventListener('change', makeCalendarFromSelectDataByYear);
    calendarEl.querySelector('[data-month]').removeEventListener('change', makeCalendarFromSelectDataByMonth);
    _ref1 = calendarEl.querySelectorAll('[data-date]');
    for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
      el = _ref1[_j];
      el.removeEventListener('mousedown', selectDateFromData);
    }
    return calendarBackdropEl.removeEventListener('click', hideCalendar);
  };
  makeCalendarFromData = function(e) {
    var date, parts;
    date = e.currentTarget.getAttribute('data-navi');
    parts = date.split('-');
    return makeCalendar(parts[0], parts[1]);
  };
  makeCalendarFromSelectDataByYear = function(e) {
    var month, year;
    year = parseInt(e.currentTarget.getAttribute('data-year'), 10);
    month = parseInt(e.currentTarget.value, 10);
    return makeCalendar(year, month);
  };
  makeCalendarFromSelectDataByMonth = function(e) {
    var month, year;
    year = parseInt(e.currentTarget.value, 10);
    month = parseInt(e.currentTarget.getAttribute('data-month'), 10);
    return makeCalendar(year, month);
  };
  selectDateFromData = function(e) {
    var date, parts;
    date = e.currentTarget.getAttribute('data-date');
    parts = date.split('-');
    return selectDate(parts[0], parts[1], parts[2]);
  };
  selectDate = function(year, month, day) {
    focusedEl.value = "" + year + "-" + month + "-" + day;
    focusedEl.dispatchEvent(event);
    return hideCalendar();
  };
  showCalendar = function(e) {
    var date_in_input, date_parts, existing_date, selected_date, styles, the_year, x, y;
    render.call(this);
    focusedEl = e.currentTarget;
    styles = focusedEl.getBoundingClientRect();
    x = styles.left;
    y = styles.top + styles.height - 1;
    calendarEl.style.left = "" + x + "px";
    calendarEl.style.top = "" + y + "px";
    calendarEl.style.display = 'block';
    calendarBackdropEl.style.display = 'block';
    existing_date = new Date();
    date_in_input = focusedEl.value;
    if (date_in_input) {
      selected_date = false;
      date_parts = date_in_input.split('-');
      if (date_parts.length === 3) {
        date_parts[1]--;
        selected_date = new Date(date_parts[0], date_parts[1], date_parts[2]);
      }
      if (selected_date && !isNaN(selected_date.getYear())) {
        existing_date = selected_date;
      }
    }
    the_year = existing_date.getYear();
    if (the_year < 1900) {
      the_year += 1900;
    }
    makeCalendar(the_year, existing_date.getMonth(), existing_date.getDate());
  };
  hideCalendar = function() {
    undelegateEvents();
    calendarEl.style.display = 'none';
    calendarBackdropEl.style.display = 'none';
  };
  render = function() {
    if (!calendarEl) {
      calendarBackdropEl = document.createElement('div');
      calendarBackdropEl.className = calendarBackdropClassName;
      document.body.appendChild(calendarBackdropEl);
      calendarEl = document.createElement('div');
      calendarEl.className = calendarClassName;
      document.body.appendChild(calendarEl);
    }
  };
  window.BnzCalendar = Calendar;
})();
