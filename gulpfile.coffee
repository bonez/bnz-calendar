gulp      = require 'gulp'
jade      = require 'gulp-jade'
stylus    = require 'gulp-stylus'
notify    = require 'gulp-notify'
coffee    = require 'gulp-coffee'
rename    = require 'gulp-rename'
concat    = require 'gulp-concat'
rupture   = require 'rupture'

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

rootPath   = '.'
assetsPath = "#{rootPath}/assets"
publicPath = "#{rootPath}/public"

paths = do ->

    paths =

        coffee:         [['coffee/app/**/*.coffee'], 'js']
        html:           ['jade/*.jade']
        stylus:         ['stylus/**/*.styl', 'css']

    for name, arr of paths
        if name isnt 'copyFonts'
            if typeof arr[0] is 'object' then for a, i in arr[0]
                arr[0][i] = if /^!/.test a then "!#{assetsPath}/#{a.substring 1}" else "#{assetsPath}/#{a}"
            else arr[0] = "#{assetsPath}/#{arr[0]}"
        arr[1] = "#{publicPath}/#{if arr[1]? then "#{arr[1]}/" else ''}"

    paths


commonTasks = (path for path of paths when not /^copy/.test path)

ntf = (title) -> notify.onError (error) -> "\n\n::: #{title} :::\n\n#{error.message}\n\n"


gulp.task 'coffee', ->
    coffeeTask = coffee bare: true
    coffeeTask.setMaxListeners 0
    gulp.src paths['coffee'][0]
        .pipe coffee bare: true
        .on 'error', -> @emit 'end'
        .on 'error', ntf 'COFFEE'
        .pipe gulp.dest paths['coffee'][1]

gulp.task 'html', ->
    gulp.src paths['html'][0]
        .pipe jade pretty: true
        .on 'error', -> @emit 'end'
        .on 'error', ntf 'JADE HTML'
        .pipe gulp.dest paths['html'][1]


gulp.task 'stylus', ->
    tmpFolderName = 'tmp'

    gulp.src paths['stylus'][0]
        .pipe stylus use:[do rupture]
        .on 'error', -> @emit 'end'
        .on 'error', ntf 'STYLUS'
        .pipe rename (path) ->
            if path.basename is '_index' then path.basename = 'styles'
            else path.dirname = if path.dirname is '.' then "/#{tmpFolderName}" else "/#{tmpFolderName}/#{path.dirname}"
            path
        .pipe gulp.dest paths['stylus'][1]


gulp.task 'build', ->
    gulp.src "#{assetsPath}/coffee/app/calendar.coffee"
        .pipe coffee bare: true
        .pipe concat 'bnz-calendar.js'
        .pipe gulp.dest "#{rootPath}"

    gulp.src "#{assetsPath}/stylus/calendar.styl"
        .pipe stylus use:[do rupture]
        .pipe concat 'bnz-calendar.css'
        .pipe gulp.dest "#{rootPath}"


gulp.task 'vendors', ->
    gulp.src [
        "#{rootPath}/node_modules/bnz-define-require/index.js"
    ]
        .pipe concat 'vendors.js'
        .pipe gulp.dest "#{publicPath}/js/"


gulp.task 'watch', -> for task in commonTasks then gulp.watch paths[task][0], [task]

gulp.task 'default', commonTasks.concat ['watch', 'vendors']
