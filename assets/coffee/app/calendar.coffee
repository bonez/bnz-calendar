# bnz-calendar
# 2014-12-26 00:04
# bonez

do ->
    'use strict'

    month_names = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

    weekdays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']

    month_days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    calendarClassName = 'bnz-calendar'

    calendarBackdropClassName = "#{calendarClassName}-backdrop"

    today = new Date()

    # HELPER: create element
    ce = (tagName, className, html) ->
        el = document.createElement tagName
        if className then el.className = className
        if html then el.innerHTML = html
        el

    focusedEl = null

    calendarEl = null

    calendarBackdropEl = null

    disabledFrom = null
    disabledTill = null

    event = null


    # how many days between date1 and date2 (dates formatted as yyyy-mm-dd)
    dateDiff = (date1, date2) ->
        Math.ceil ((new Date date2).getTime() - (new Date date1).getTime()) / (1000 * 3600 * 24)


    # @param {date} - accepts date in string format (like 2015-01-23)
    getDateAfterSomeDays = (date, some) ->
        if typeof date is 'number'
            myDate = new Date @getDateFormatted()
            some = date
        else myDate = new Date date
        myDate.setDate(myDate.getDate() + some);
        @getDateFormatted myDate

    getTomorrowDate = (date = @getDateFormatted()) -> getDateAfterSomeDays.call @, date, 1

    getYesterdayDate = (date = @getDateFormatted()) -> getDateAfterSomeDays.call @, date, -1


    class Calendar

        constructor: (options = {
            selector: '.datepicker'
            disabledFrom: null
            disabledTill: null
        }) ->

            # Subscribe to this event
            #
            # document.addEventListener 'bnzCalendarDateChange', (e) ->
            #     console.log e.target.value
            # , no
            #
            event = new CustomEvent 'bnzCalendarDateChange',
                bubbles: yes
                cancelable: yes

            disabledFrom = switch options.disabledFrom
                when 'today' then @getDateFormatted()
                when 'tomorrow' then getTomorrowDate.call @
                when 'yesterday' then getYesterdayDate.call @
                else options.disabledFrom

            disabledTill = if typeof options.disabledTill is 'number'
                getDateAfterSomeDays.call @, disabledFrom, options.disabledTill
            else options.disabledTill

            inputs = document.querySelectorAll options.selector
            if inputs.length <= 0 then return

            timer = null

            for input in inputs
                input.addEventListener 'focus', (e) =>
                    window.clearTimeout timer
                    showCalendar.call @, e
                , no
                # input.addEventListener 'blur', =>
                #     timer = window.setTimeout hideCalendar, 1
                # , no
                input.addEventListener 'keydown', (e) =>
                    # prevent if not tab key
                    if e.keyCode isnt 9 then e.preventDefault()
                    # esc key
                    if e.keyCode is 27 then hideCalendar(); e.target.blur()
                , no
            return


        # format: yyyy-mm-dd
        getDateFormatted: (d = new Date()) ->
            year = d.getFullYear().toString()
            mm = (d.getMonth() + 1).toString()
            mm = if mm[1] then mm else "0#{mm[0]}"
            dd = d.getDate().toString()
            dd = if dd[1] then dd else "0#{dd[0]}"
            "#{year}-#{mm}-#{dd}"


        dateDiff: dateDiff

        getDateAfterSomeDays: getDateAfterSomeDays

        getTomorrowDate: getTomorrowDate

        getYesterdayDate: getYesterdayDate



    makeCalendar = (year, month, day) ->
        year  = parseInt year, 10
        month = parseInt month, 10
        day   = parseInt day, 10

        next_month = month + 1
        next_month_year = year

        if next_month >= 12
            next_month = 0
            next_month_year++

        previous_month = month - 1
        previous_month_year = year

        if previous_month < 0
            previous_month = 11
            previous_month_year--


        # Clear all first
        calendarEl.innerHTML = ''


        # Navi container
        naviDiv = ce 'div', 'navi'
        calendarEl.appendChild naviDiv


        # Previous button
        naviPrev = ce 'div', null, '&lt;'
        naviPrev.setAttribute 'data-navi', "#{previous_month_year}-#{previous_month}"
        naviPrev.setAttribute 'title', "#{month_names[previous_month]} #{previous_month_year}"
        naviDiv.appendChild naviPrev


        # Selects container
        selectsDiv = ce 'div', 'calendar-title'
        naviDiv.appendChild selectsDiv


        # Next button
        naviNext = ce 'div', null, '&gt;'
        naviNext.setAttribute 'data-navi', "#{next_month_year}-#{next_month}"
        naviNext.setAttribute 'title', "#{month_names[next_month]} #{next_month_year}"
        naviDiv.appendChild naviNext


        # Month select
        selectMonth = ce 'select', 'calendar-month'
        selectMonth.setAttribute 'data-year', year
        selectsDiv.appendChild selectMonth

        for value, index in month_names
            option = ce 'option', null, value
            option.value = index
            if index is month then option.setAttribute 'selected', 'selected'
            selectMonth.appendChild option


        # Year select
        selectYear = ce 'select', 'calendar-year'
        selectYear.setAttribute 'data-month', month
        selectsDiv.appendChild selectYear

        current_year = today.getYear()
        if current_year < 1900 then current_year += 1900
        i = current_year - 70

        while i < current_year + 15
            option = ce 'option', null, i
            option.value = i
            if i is year then option.setAttribute 'selected', 'selected'
            selectYear.appendChild option
            i++


        # Header
        header = ce 'div', 'header'
        calendarEl.appendChild header

        weekday = 0
        while weekday < 7
            week = ce 'div', null, weekdays[weekday]
            header.appendChild week
            weekday++


        # Get the first day of this month
        first_day = new Date year, month, 0
        start_day = first_day.getDay()
        d = 1
        flag = 0

        # Leap year support
        month_days[1] = if year % 4 is 0 then 29 else 28

        days_in_this_month = month_days[month]

        # Create the calendar

        i = 0

        while i <= 5
            if w >= days_in_this_month then break
            weekEl = ce 'div', 'week'
            calendarEl.appendChild weekEl
            j = 0

            while j < 7

                # If the days has overshooted the number of days in this month, stop writing
                if d > days_in_this_month
                    flag = 0

                else

                    # If the first day of this month has come, start the date writing
                    if j >= start_day and not flag
                        flag = 1

                if flag
                    w = d
                    w0 = w
                    mon = month + 1
                    if w0 < 10 then w0 = "0#{w}"
                    if mon < 10 then mon = "0#{mon}"

                    # Is it today?
                    class_name = weekdays[j].toLowerCase()
                    yea = today.getYear()
                    if yea < 1900 then yea += 1900
                    if yea is year and today.getMonth() is month and today.getDate() is d then class_name = ' today'
                    if day is d then class_name += ' selected'

                    currDate = "#{yea}-#{mon}-#{if "#{d}"[1] then d else "0#{"#{d}"[0]}"}"

                    if 0 <= dateDiff currDate, disabledFrom then class_name += ' disabled'

                    if 0 > dateDiff currDate, disabledTill then class_name += ' disabled'


                    dayEl = ce 'div', class_name, w
                    dayEl.setAttribute 'data-date', "#{year}-#{mon}-#{w0}"
                    weekEl.appendChild dayEl

                    d++
                else
                    emptyDiv = ce 'div', null, '&nbsp;'
                    weekEl.appendChild emptyDiv
                j++

            i++

        delegateEvents.call @
        return


    delegateEvents = ->

        for el in calendarEl.querySelectorAll '[data-navi]'
            el.addEventListener 'click', makeCalendarFromData, no

        calendarEl.querySelector '[data-year]'
            .addEventListener 'change', makeCalendarFromSelectDataByYear, no

        calendarEl.querySelector '[data-month]'
            .addEventListener 'change', makeCalendarFromSelectDataByMonth, no

        for el in calendarEl.querySelectorAll '[data-date]:not(.disabled)'
            el.addEventListener 'mousedown', selectDateFromData, no

        calendarBackdropEl.addEventListener 'click', hideCalendar


    undelegateEvents = ->

        for el in calendarEl.querySelectorAll '[data-navi]'
            el.removeEventListener 'click', makeCalendarFromData

        calendarEl.querySelector '[data-year]'
            .removeEventListener 'change', makeCalendarFromSelectDataByYear

        calendarEl.querySelector '[data-month]'
            .removeEventListener 'change', makeCalendarFromSelectDataByMonth

        for el in calendarEl.querySelectorAll '[data-date]'
            el.removeEventListener 'mousedown', selectDateFromData

        calendarBackdropEl.removeEventListener 'click', hideCalendar


    makeCalendarFromData = (e) ->
        date = e.currentTarget.getAttribute 'data-navi'
        parts = date.split '-'
        makeCalendar parts[0], parts[1]


    makeCalendarFromSelectDataByYear = (e) ->
        year = parseInt (e.currentTarget.getAttribute 'data-year'), 10
        month = parseInt (e.currentTarget.value), 10
        makeCalendar year, month


    makeCalendarFromSelectDataByMonth = (e) ->
        year = parseInt (e.currentTarget.value), 10
        month = parseInt (e.currentTarget.getAttribute 'data-month'), 10
        makeCalendar year, month


    selectDateFromData = (e) ->
        date = e.currentTarget.getAttribute 'data-date'
        parts = date.split '-'
        selectDate parts[0], parts[1], parts[2]


    selectDate = (year, month, day) ->
        focusedEl.value = "#{year}-#{month}-#{day}"
        focusedEl.dispatchEvent event
        hideCalendar()


    showCalendar = (e) ->
        render.call @

        focusedEl = e.currentTarget

        styles = focusedEl.getBoundingClientRect()

        x = styles.left
        y = styles.top + styles.height - 1

        calendarEl.style.left = "#{x}px"
        calendarEl.style.top = "#{y}px"
        calendarEl.style.display = 'block'

        calendarBackdropEl.style.display = 'block'

        existing_date = new Date()
        date_in_input = focusedEl.value

        if date_in_input
            selected_date = no
            date_parts = date_in_input.split '-'

            if date_parts.length is 3
                date_parts[1]-- # Month starts with 0
                selected_date = new Date date_parts[0], date_parts[1], date_parts[2]

            # Valid date.
            if selected_date and not isNaN selected_date.getYear()
                existing_date = selected_date

        the_year = existing_date.getYear()
        if the_year < 1900 then the_year += 1900

        makeCalendar the_year, existing_date.getMonth(), existing_date.getDate()

        return


    hideCalendar = ->
        undelegateEvents()
        calendarEl.style.display = 'none'
        calendarBackdropEl.style.display = 'none'
        return


    render = ->
        if not calendarEl
            calendarBackdropEl = document.createElement 'div'
            calendarBackdropEl.className = calendarBackdropClassName
            document.body.appendChild calendarBackdropEl

            calendarEl = document.createElement 'div'
            calendarEl.className = calendarClassName
            document.body.appendChild calendarEl
        return

#    if typeof define is 'function'
#        define 'BnzCalendar', (module) -> module.exports = Calendar
#    else
    window.BnzCalendar = Calendar

    return
